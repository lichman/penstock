extern crate actix_web;
extern crate actix_files;
extern crate graceful;
extern crate pretty_env_logger;
#[macro_use]
extern crate log;
extern crate clap;

mod api;
mod pipe;
mod send;
mod inspect;

use structopt::clap::AppSettings;
use structopt::StructOpt;
use std::sync::atomic::{AtomicBool, Ordering};
use graceful::SignalGuard;
use pipe::{KafkaPipe, KafkaPipeOpt};
use send::{KafkaSend, KafkaSendOpt};
use std::thread;
use std::process::exit;


#[derive(StructOpt, Debug)]
#[structopt(name = "penstock",
author = "Andrew Lichman <andrew.lichman@gmail.com>",
about = "Tool for transfer data between kafka clusters",
global_settings = & [AppSettings::DeriveDisplayOrder, AppSettings::ColoredHelp]
)]
enum Opt {
    #[structopt(about = "Transfer data between kafka clusters")]
    Kafka(KafkaPipeOpt),

    #[structopt(about = "Send data to  kafka topic")]
    Send(KafkaSendOpt),

    #[structopt(about = "Show information about kafka cluster")]
    Inspect,
}

pub static STOP: AtomicBool = AtomicBool::new(false);

fn main() {
    let signal_guard = SignalGuard::new();
    let mut log_builder = pretty_env_logger::formatted_timed_builder();

    if let Ok(s) = ::std::env::var("PENSTOCK_LOG_LEVEL") {
        log_builder.parse_filters(&s);
    } else {
        log_builder.parse_filters("INFO");
    }
    log_builder.default_format_module_path(false);
    log_builder.init();


    let opt = Opt::from_args();


    let app = match opt {
        //   Opt::Kafka(cfg) => thread::spawn(|| { kafka::main::run(cfg) }),
        Opt::Kafka(cfg) => thread::spawn(|| {
            KafkaPipe::new(cfg).run();
            exit(0);
        }),
        Opt::Send(cfg) => thread::spawn(|| {
            KafkaSend::new(cfg).run();
            exit(0);
        }),
        Opt::Inspect => thread::spawn(|| {
           error!("Not implemented yet");
            exit(1);

        }),
    };


    signal_guard.at_exit(move |_sig| {
        STOP.store(true, Ordering::Release);
        app.join().unwrap();
    });
}
