pub fn parse_hosts(src: String) -> Vec<String> {
    src.split(",").map(|p| { String::from(p) }).collect()
}
