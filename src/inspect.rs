use kafka::client::KafkaClient;

pub struct KafkaInspect {
    client: KafkaClient

}


impl KafkaInspect {
    pub fn _new() -> Self {
        let mut client = KafkaClient::new(vec!["1".to_owned()]);
        client.set_client_id("test".to_owned());

        KafkaInspect {
            client
        }
    }

    pub fn _run(self) {
        let mut client = self.client;
        client.load_metadata_all().unwrap();

        for topic in client.topics() {
            print!("\n topic: {}  = ", topic.name());
            for id in topic.partitions().available_ids() {
                print!("{} ", id);
            }
        }
    }
}
