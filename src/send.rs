use std::time::Duration;
use kafka::producer::*;
use structopt::StructOpt;

use crate::api::parse_hosts;

pub struct KafkaSend {
    producer: Producer,
    opts: KafkaSendOpt,
}

impl KafkaSend {
    pub fn new(opts: KafkaSendOpt) -> Self {
        let producer = Producer::from_hosts(opts.get_hosts())
            .with_client_id("id".to_owned())
            .with_ack_timeout(Duration::from_secs(1))
            .with_required_acks(RequiredAcks::One)
            .create()
            .expect("error_message.as_str()");

        Self { producer, opts }
    }

    pub fn run(self) {
        let mut producer = self.producer;
        let opts = &self.opts;
        let topic = opts.topic.as_str();
        let key = opts.key.as_bytes();
        let message = opts.message.as_bytes();

        let message = Record::from_key_value(topic, key, message)
            .with_partition(opts.part.clone());

        match producer.send(&message) {
            Ok(_r) => println!("Message sent"),
            Err(e) => println!("Failed to send message! Error {}", e),

        }
    }
}


#[derive(StructOpt, Debug)]
pub struct KafkaSendOpt {
    #[structopt(
    long_help = r"Kafka servers to witch will be sent data
Host names or IP Addresses with port divided by comma
Example:
 - 127.0.0.1:9092,127.0.0.2:9092
 - data-1.local:9092,data-2.local:9092"
    )]
    hosts: String,

    #[structopt(
    long_help = r"Topic to witch will be sent data"
    )]
    pub topic: String,

    #[structopt(
    long_help = r"Partition to witch will be sent data.
List of integers divided by comma
Example: 1"
    )]
    part: i32,

    #[structopt(
    long_help = r"Partition to witch will be sent data.
List of integers divided by comma
Example: 1,2,3"
    )]
    key: String,

    #[structopt(
    long_help = r"Partition to witch will be sent data.
List of integers divided by comma
Example: 1,2,3"
    )]
    message: String,

}

impl KafkaSendOpt {
    pub fn get_hosts(&self) -> Vec<String> {
        parse_hosts(self.hosts.clone())
    }
}