use actix_web::*;
use actix_web::client::*;
use actix_web::web::{Payload, Bytes};
use actix_web::error::ErrorBadGateway;
use std::ops::Add;
use actix_http::body::*;

//type ProxyResponse = Box<dyn Future<Item=HttpResponse, Error=Error>>;

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg
        .service(
            web::resource("/*")
                .route(web::to(forward))
                .route(web::head().to(|| HttpResponse::MethodNotAllowed())),
        );
}


async fn forward(req: HttpRequest, body: Bytes) -> Result<HttpResponse, Error> {
    let urls: Vec<String> = vec![
        "http://localhost:9999".to_owned(),
        "http://localhost:8080".to_owned()
    ];
   dbg!(&req.uri()); dbg!(&req.method());

    for url in urls {
        dbg!(&url);
        if let Ok(resp) = request(url, req.clone(), body.clone()).await
        { return Ok(resp); };
    };

    Ok(HttpResponse::BadGateway().finish())

}


async fn request(url: String, req: HttpRequest, body: Bytes) -> Result<HttpResponse, Error> {
    let client = Client::default();

    let path = String::from(url).add( &req.uri().to_string());
    let mut request = client.request_from(path, req.head());
    let response = request.send_body(body.to_vec()).await;

    if let Ok(resp) = &response {
        if resp.status().is_server_error() { return Err(ErrorBadGateway("")); }
    } else if let Err(_e) = &response {
        return Err(ErrorBadGateway(""));
    }

    response
        .map(|res| {
            let mut client_resp = HttpResponse::build(res.status());

            for (header_name, header_value) in res
                .headers()
                .iter()
                .filter(|(h, _)| *h != "connection" && *h != "content-length")
                {
                    client_resp.header(header_name.clone(), header_value.clone());
                }
            client_resp.streaming(res)
        })
        .map_err(|_| { ErrorBadGateway("") })
}