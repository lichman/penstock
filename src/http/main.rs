use actix_web::{App, HttpServer};


use crate::http::{proxy, files};

pub fn run() -> Result<(),std::io::Error>  {

    let server = HttpServer::new(||
        App::new()
            .configure(proxy::config)
            .configure(files::config)
    );

    println!("START");

    server
        .bind("0.0.0.0:8000")
        .expect("Can not bind to port 8000")
        .run()
}