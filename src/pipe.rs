use kafka::consumer::{Consumer, FetchOffset, GroupOffsetStorage, MessageSet};
use kafka::producer::*;
use std::time::Duration;
use std::sync::atomic::{Ordering};
use crate::STOP;
use structopt::StructOpt;
use crate::api::parse_hosts;
use std::process::exit;


pub struct KafkaPipe {
    consumer: Consumer,
    producer: Producer,
    opts: KafkaPipeOpt,
}


impl KafkaPipe {
    pub fn new(opts: KafkaPipeOpt) -> Self {
        let consumer = Self::consumer(opts.from_topic.clone(), opts.get_from_hosts(), opts.get_penstock_id(), opts.id.clone(), opts.get_parts());
        let producer = Self::producer(opts.get_to_hosts(), opts.get_penstock_id());
        Self { consumer, producer, opts }
    }

    pub fn run(self) {
        let mut consumer = self.consumer;
        let mut producer = self.producer;

        let opts = &self.opts;


        info!("Start pipe {} ==> {}", opts.get_from(), opts.get_to());
        while !STOP.load(Ordering::Acquire) {
            match consumer.poll() {
                Ok(data) => {
                    for ms in data.iter() {
                        let partition_index = ms.partition();
                        let message_count = ms.messages().len();

                        let batch: Vec<Record<&[u8], &[u8]>> = (&ms as &MessageSet).messages().iter()
                            .map(|m| {
                                Record::from_key_value(opts.to_topic.as_str(), m.key.clone(), m.value.clone()).with_partition(partition_index)
                            }).collect();

                        match producer.send_all(batch.as_slice()) {
                            Err(e) => {
                                error!("Send to kafka topic {} failed with error: {}", opts.to_topic, e);
                                exit(1);
                            }
                            Ok(v) => {
                                for r in v {
                                    for tpc in r.partition_confirms {
                                        if let Err(code) = tpc.offset {
                                            error!("Commit to topic {} failed with code: {:?} ", opts.to_topic, code);
                                            exit(1);
                                        }
                                    }
                                }
                            }
                        }


                        consumer.consume_messageset(ms).unwrap();
                        consumer.commit_consumed().unwrap();
                        let last_consumed_message = consumer.last_consumed_message(opts.from_topic.clone().as_str(), partition_index).unwrap();
                        info!("last committed message offset {}, consumed {} messages for partition {}",
                              last_consumed_message,
                              message_count,
                              partition_index
                        );
                    }
                }
                Err(e) => {
                    error!("Read from kafka topic {} failed with error: {}", opts.from_topic, e);
                    exit(1);
                }
            }
        }
    }

    fn consumer(topic: String, hosts: Vec<String>, group_id: String, id: String, partitions: Vec<i32>) -> Consumer {
        let hosts_str = hosts.concat();
       let consumer = Consumer::from_hosts(hosts.clone())
            .with_topic_partitions(topic.clone(), partitions.as_slice())
            .with_fallback_offset(FetchOffset::Earliest)
            .with_fetch_max_bytes_per_partition(1048576)
            .with_group(group_id.clone())
            .with_client_id(id)
            .with_connection_idle_timeout(Duration::from_millis(1000))
            .with_offset_storage(GroupOffsetStorage::Kafka)
            .create();

        match consumer{
            Ok(c) => c,
            Err(e) => {
                error!("Consumer {} {} failed to start with error {}", hosts_str, group_id , e);
                exit(1);
            }
        }
    }


    fn producer(hosts: Vec<String>, id: String) -> Producer {
        let hosts_str = hosts.concat();
        let producer = Producer::from_hosts(hosts)
            .with_client_id(id.clone())
            .with_ack_timeout(Duration::from_secs(1))
            .with_required_acks(RequiredAcks::One)
            .create();

        match producer {
            Ok(p) => p,
            Err(e) => {
                error!("Producer {} {} failed to start with error {}", hosts_str, id, e);
                exit(1);
            }
        }

    }
}


#[derive(StructOpt, Debug)]
pub struct KafkaPipeOpt {
    #[structopt(
    long_help = r"Kafka servers from witch data will be downloaded
Host names or IP Addresses with port divided by comma
Example:
 - 127.0.0.1:9092,127.0.0.2:9092
 - data-1.local:9092,data-2.local:9092"
    )]
    from_kafka: String,

    #[structopt(
    long_help = r"Topic from witch data will be downloaded"
    )]
    pub from_topic: String,

    #[structopt(
    long_help = r"Partitions from with data will be downloaded.
List of integers divided by comma
Example: 1,2,3"
    )]
    from_parts: String,

    #[structopt(
    long_help = r"Kafka servers to witch data will be uploaded
Host names or IP Addresses with port divided by comma
Example:
 - 127.0.0.1:9092,127.0.0.2:9092
 - data-1.local:9092,data-2.local:9092"
    )]
    to_kafka: String,

    #[structopt(
    long_help = r"Topic to witch data will be uploaded"
    )]
    pub to_topic: String,

    #[structopt(
    long,
    default_value = "penstock_id",
    long_help = r"Set an optional identifier of a Kafka consumer (in a consumer group)
that is passed to a Kafka broker with every request.
The sole purpose of this is to be able to track the source of requests beyond just ip and port by allowing
a logical application name to be included in Kafka logs and monitoring aggregates.
"
    )]
    pub id: String,
}

impl KafkaPipeOpt {
    pub fn get_from_hosts(&self) -> Vec<String> {
        parse_hosts(self.from_kafka.clone())
    }

    pub fn get_to_hosts(&self) -> Vec<String> {
        parse_hosts(self.to_kafka.clone())
    }


    pub fn get_parts(&self) -> Vec<i32> {
        self.from_parts.split(",").map(|p| { p.parse().unwrap() }).collect()
    }

    pub fn get_penstock_id(&self) -> String {
        String::from(["penstock.<", self.to_kafka.as_str(), ">.", self.to_topic.as_str()].concat())
    }

    pub fn get_from(&self) -> String {
        String::from([self.from_kafka.as_str(), self.from_topic.as_str(), self.from_parts.as_str()].join("."))
    }

    pub fn get_to(&self) -> String {
        String::from([self.to_kafka.as_str(), self.to_topic.as_str()].join("."))
    }
}

